import argparse
import traceback 
import sys
import os
import re

def main(filename='', precision=3, output=''):
    if not output:
        dirname = os.path.dirname(filename)
        if dirname:
            output = '_' + filename[len(dirname) + 1:]
        else:
            output = '_' + filename[len(dirname) + 1:]
    
    try:
        txt = open(filename, 'rt').read()
        nums = re.findall('(\d+\.\d\d+)', txt)
        templ = '{0:.' + str(precision) + 'f}'
        trans = {x:templ.format(float(x)) for x in nums}

        for old_val, new_val in trans.items():
            txt = txt.replace(old_val, new_val)

        open(output, 'wt').write(txt)

    except Exception:
        traceback.print_exc() 
        return 1

    return 0
             

def check_filename(parser, filename):
    if not os.path.exists(filename):
       parser.error(f"filename {filename} doesn't exists")
    return filename


def check_precision(parser, precision):
    if not (precision.isnumeric() and precision.isdecimal()):
        parser.error(f'precision "{precision}" is not an integer number <= 8')

    if int(precision) > 8:
       parser.error(f"too many decimals number. Max. = 8")
    return precision


if __name__ == '__main__':
    name, *args = sys.argv
    
    parser = argparse.ArgumentParser()

    parser.add_argument(dest='filename', 
        type=lambda fn:check_filename(parser,fn),
        help="file to process"
    )
    parser.add_argument('-p', '--precision', 
        dest='precision', default=3,
        type=lambda num:check_precision(parser,num),
        help='number of decimals desired. Default = 3.'
    )
    parser.add_argument('-o', '--output',
        dest='output', type=str, default='',
        help='output filename'
    )

    args = parser.parse_args()
    main(**vars(args))
