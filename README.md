# SVG imported from external URI

## Description

This is a very simple project showing how to load SVG definitions from external URI, leaving all responsibility of rendering time to browser and leaving SVG elements accessible from CSS.

You can see this code working [here](https://imanolvalero.gitlab.io/external-svgs).

## Motivation

Yesterday (March 19th, 2021), on a live coding session of [Daniel Primo](https://danielprimo.io), with the rest The Malandriners, Daniel made us a short introduction to SVG documents, coding a simple and cute web page.

The SVG code was inline and if defined path is complex, it could result in a ugly HTML document for human reading, at least for my taste. I was not comfortable with that.

So, I have made a tiny reach  for using SVG documents in a website leaving the code as much readable as possible.

This is the result of a couple of hours, and as I do not use SVG everyday, I leave here the results of this tiny research, for future consults.

__Thanks to Daniel Primo and to The Malandriners.__ Getting in touch with them has encouraged me to make this tiny thing public.

### Learned lessons

* SVG is not a image, it is a document with [its elements](https://developer.mozilla.org/en-US/docs/Web/SVG/Element).
* If you add SVG documents in backgrounds, or via `img`, `embed`, `object` or `iframe` tags, you cannot modify them via CSS.
* If you load them via javascript, this will penalize the document loading time.

### Goals

* Keep HTML document less ugly for human read.
* Decouple SVG definitions from HTML document.
* Maintain the SVG document accessible from CSS.
* Avoid using javascript.

### Cons

* This technique could break some cross-domain security policy if SVGs are external.  
This can be avoided pointing only to locally stored files.
* If external file have more shapes than will be used in document, unused shapes will be loaded. This would penalize the document load time.  
This can be avoided using one field for one shape or symbol.

## Optimization tool

In `tools` folder I have saved a quick and dirty pytohn script for fix the decimals of a file to a desired number of decimals.  
In this way we can reduce a SVG file created by a software like InkScape.

## References

* [W3C  - SVG 1.1 - `<use>` element](https://www.w3.org/TR/SVG11/struct.html#UseElement)
* [MDN Web Docs - SVG - `<use>` element](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/use)
* [CSS-TRICKS - SVG \`use\` with External Source](https://css-tricks.com/svg-use-external-source/)
* [GitHub.blog - Delivering Octoicons with SVG](https://github.blog/2016-02-22-delivering-octicons-with-svg/)
